/*
 * test_RS485.h
 *
 *  Created on: 23 sept. 2021
 *      Author: luis
 */

#ifndef MAIN_INCLUDE_TEST_RS485_H_
#define MAIN_INCLUDE_TEST_RS485_H_
#include "stdbool.h"
#include "stdint.h"

#define CLOSE_RELAY 18
#define OPEN_RELAY  23

void test_RS485_RH18(void* arg);
void modbus_master_poll(void* arg);

enum modbus_function_t {
    READ_HOLDING = 3,
    READ_INPUT,
    WRITE_SINGLE_COIL,
    WRITE_MULTIPLES_COILS = 0x0f
};

/**
 * @brief Union to separate the data from the different poll types
 * */
typedef union {
	bool coil_state; /**< For coil function*/
	uint16_t starting_addrs; /**< For reading registers*/
}modbus_data_t;

/**
 * @brief Type to transmit information to the modbus poll task
 * */
typedef struct {
	uint8_t slave;
	uint8_t function;
	modbus_data_t data;
}
modbus_poll_event_t;
/**
 * @brief Function to read a series of input register in a external slave
 *
 * @param[in] slave Slave which is been polled
 * @param[in] start_address Starting address of the registers
 * @param[in] quantity Quantity of registers to be polled after {@link
 * start_address}
 * */
void read_input_register(uint8_t slave, uint16_t start_address,
                         uint16_t quantity);
/**
 * @brief Function to save a series of input registers read in a slave
 *
 * @param[in] data Pointer to the data of the modbus frame
 * @param[in] length Length of the data
 * @param[in] modbus_registers Pointer to the pointes of the modbus registers
 * */
void save_register(uint8_t *data, uint8_t length, uint16_t **modbus_registers);

/**
 * @brief Function to check exception in the incoming data from a slave
 *
 * @param[in] data Pointer to the data of the modbus frame
 * */
int check_exceptions(uint8_t *data);
/**
 * @brief Function to start the Slave array used when the masters modbus polls
 *
 * @param[out] slaves Pointer to the array of bools which inidices represents
 * the slaves
 * */
void init_slaves(bool *slaves);

void write_single_coil(uint8_t slave, bool coil_state);

#endif /* MAIN_INCLUDE_TEST_RS485_H_ */
