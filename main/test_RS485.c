/*
 * test_RS485.c
 *
 *  Created on: 23 sept. 2021
 *      Author: luis
 */
#include "esp_log.h"
#include "test_led.h"
#include "esp_task_wdt.h"
#include "flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"
#include "global_variables.h"
#include "nvs.h"
#include "nvs_flash.h"
//#include "ota_update.h"
#include "string.h"
#include <stdio.h>
#include <stdlib.h>
#include <test_led.h>
#include <test_LoRa.h>
#include <test_RS485.h>
#include "Config_UART.h"
#include "global_variables.h"
#include "CRC.h"

#define RTS_PIN 27
#define TXD_PIN     33
#define RXD_PIN     26
#define CTS_PIN     3    // ORIGINAL PIN 2

#define SLAVES 10

#define RX_BUF_SIZE 1024
#define TX_BUF_SIZE 1024

#define TWDT_TIMEOUT_S 20
#define TWDT_RESET     5000
#define MODBUS_TIMEOUT 2000 // in ticks == 1 s

static char *TAG      = "INFO";
static char *TAG_UART = "MODBUS";


#define MAX_POLL_QUEUE_SIZE 5  // Max quantity of polls in queue
static uint16_t *modbus_registers[4];
static uint16_t inputRegister[512] = {0};
QueueHandle_t uart_send_queue;

bool modbus_coils[512];
typedef union {
    uint16_t Val;
    struct {
        uint8_t LB;
        uint8_t HB;
    } byte;

} INT_VAL;
typedef union {
    uint32_t doubleword;
    struct {
        uint16_t wordL;
        uint16_t wordH;
    } word;

} WORD_VAL;

enum modbus_excep_t {
    ILLEGAL_FUNC = 1,
    ILLEGAL_DATA_AD,
    ILLEGAL_DATA,
    SLAVE_FAIL,
    ACK,
    BUSY_SLAVE,
    NEG_ACK,
    PAR_ERR
};

#define CHECK_ERROR_CODE(returned, expected)                                   \
    ({                                                                         \
        if (returned != expected) {                                            \
            ESP_LOGE(TAG, "TWDT ERROR\n");                                     \
            abort();                                                           \
        }                                                                      \
    })

void uart_init(QueueHandle_t *queue) {
    uart_driver_delete(UART_NUM_1);
    int uart_baudarate              = 9600;
    const uart_config_t uart_config = {
        .baud_rate = uart_baudarate,
        .data_bits = UART_DATA_8_BITS,
        .parity    = UART_PARITY_EVEN,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
    };
    uart_param_config(UART_NUM_1, &uart_config);
    uart_set_pin(UART_NUM_1, TXD_PIN, RXD_PIN, RTS_PIN, CTS_PIN);
    uart_driver_install(UART_NUM_1, RX_BUF_SIZE * 2, TX_BUF_SIZE * 2, 20, queue,
                        0);
    uart_set_mode(UART_NUM_1, UART_MODE_RS485_HALF_DUPLEX);

    gpio_set_direction(CLOSE_RELAY, GPIO_MODE_OUTPUT);
    gpio_set_direction(OPEN_RELAY, GPIO_MODE_OUTPUT);
    gpio_set_pull_mode(CLOSE_RELAY, GPIO_PULLDOWN_ONLY);
    gpio_set_pull_mode(OPEN_RELAY, GPIO_PULLDOWN_ONLY);
}

uint8_t slave_to_change;
void write_single_coil(uint8_t slave, bool coil_state) {
    INT_VAL address;
    INT_VAL Number;
    INT_VAL CRC;

    uint8_t *frame = (uint8_t *)malloc(TX_BUF_SIZE);
    frame[0]       = slave;
    frame[1]       = 0x05;
    address.Val    = slave;
    frame[2]       = address.byte.HB;
    frame[3]       = address.byte.LB;
    if (coil_state == true) {
        Number.Val = 0xff00;
    } else {
        Number.Val = 0x0000;
    }
    frame[4] = Number.byte.HB;
    frame[5] = Number.byte.LB;
    CRC.Val  = CRC16(frame, 6);
    frame[6] = CRC.byte.LB;
    frame[7] = CRC.byte.HB;
    if (uart_write_bytes(UART_NUM_1, (const char *)frame, 8) == ESP_FAIL) {
        ESP_LOGE(TAG, "Error writing UART data");
        free(frame);
        return;
    }
    free(frame);
    ESP_LOGI(TAG, "Poll sent to slave : %d...", slave_to_change);
    slave_to_change = 0;
}

void read_input_register(uint8_t slave, uint16_t start_address,
                         uint16_t quantity) {
    INT_VAL address;
    INT_VAL Number;
    INT_VAL CRC;

    uint8_t *frame = (uint8_t *)malloc(TX_BUF_SIZE);
    frame[0]       = slave;
    frame[1]       = 0x04;
    address.Val    = start_address;
    frame[2]       = address.byte.HB;
    frame[3]       = address.byte.LB;
    Number.Val     = quantity;
    frame[4]       = Number.byte.HB;
    frame[5]       = Number.byte.LB;
    CRC.Val        = CRC16(frame, 6);
    frame[6]       = CRC.byte.LB;
    frame[7]       = CRC.byte.HB;
    if (uart_write_bytes(UART_NUM_1, (const char *)frame, 8) == ESP_FAIL) {
        ESP_LOGE(TAG, "Error writing UART data");
        free(frame);
        return;
    }
    free(frame);
    ESP_LOGI(TAG, "Poll sent to slave : %d...", slave);
}

static void check_toletance(uint8_t slave, INT_VAL *tolerance_register,
                            uint16_t *input_register) {
    WORD_VAL previous;
    WORD_VAL current;
    const uint TOLERANCE = 1000;

    previous.word.wordH = tolerance_register[0].Val;
    previous.word.wordL = tolerance_register[1].Val;
    current.word.wordH  = input_register[slave * 2 - 1];
    current.word.wordL  = input_register[slave * 2];
    uint slope          = abs(current.doubleword - previous.doubleword);
    modbus_coils[slave] = false;
    if (slope > TOLERANCE) {
        modbus_coils[slave] = true;
        ESP_LOGW(TAG, "Noise overcome the predetermined trigger [%d]",
                 modbus_coils[slave]);
    }
}

void save_register(uint8_t *data, uint8_t length, uint16_t **modbus_registers) {
    uint8_t FUNCTION        = data[1];
    uint16_t *inputRegister = modbus_registers[1];
    uint8_t SLAVE           = data[0];
    switch (FUNCTION) {
    case READ_INPUT:;
        uint8_t byte_count = data[2];
        INT_VAL aux_registro;
        INT_VAL *tolerance_register =
            (INT_VAL *)malloc((byte_count / 2) * sizeof(INT_VAL));
        for (uint8_t i = 0; i < byte_count / 2; i++) {
            aux_registro.byte.HB             = data[3 + 2 * i];
            aux_registro.byte.LB             = data[4 + 2 * i];
            tolerance_register[i].Val        = inputRegister[SLAVE * 2 + i - 1];
            inputRegister[SLAVE * 2 + i - 1] = aux_registro.Val;
        }
        check_toletance(SLAVE, tolerance_register, inputRegister);
        ESP_LOGI(TAG, "Received data saved...");
        free(tolerance_register);
        break;
    }
}

int check_exceptions(uint8_t *data) {
    uint8_t FUNCTION  = data[1];
    uint8_t SLAVE     = data[0];
    uint8_t EXCEPTION = data[2];
    if (FUNCTION == 0x04||FUNCTION == 0x05)
        return ESP_OK;
    switch (EXCEPTION) {
    case ILLEGAL_FUNC:
        ESP_LOGE(TAG, "Invalid Function %d requested in slave %d",
                 FUNCTION - 0x80, SLAVE);
        break;
    case ILLEGAL_DATA_AD:
        ESP_LOGE(TAG, "Invalid data address requested in slave %d", SLAVE);
        break;
    case ILLEGAL_DATA:
        ESP_LOGE(TAG, "Invalid data value requested in slave %d", SLAVE);
        break;
    case SLAVE_FAIL:
        ESP_LOGE(TAG,
                 "Slave  %d fail attempting to execute the requested action",
                 SLAVE);
        break;
    case ACK:
        ESP_LOGE(TAG, "Slave %d ACK, more time is needed ", SLAVE);
        break;
    case BUSY_SLAVE:
        ESP_LOGE(TAG, "Slave %d is busy, requested action rejected ", SLAVE);
        break;
    case NEG_ACK:
        ESP_LOGE(TAG,
                 "Slave %d ACK can not perform the action with the "
                 "requested query ",
                 SLAVE);
        break;
    case PAR_ERR:
        ESP_LOGE(TAG, "Slave %d received data with CRC ERROR ", SLAVE);
        break;
    default:
        ESP_LOGE(TAG, "Unknown EXCEPTION (%d) in slave %d  ", EXCEPTION, SLAVE);
    }
    return ESP_FAIL;
}

void modbus_master_poll(void *arg){

    CHECK_ERROR_CODE(esp_task_wdt_add(NULL), ESP_OK);
    CHECK_ERROR_CODE(esp_task_wdt_status(NULL), ESP_OK);

	ESP_LOGI(TAG, "Modbus Master POLL task started");

	modbus_poll_event_t* poll_event = malloc(sizeof(modbus_poll_event_t));
	if (SLAVES == 0) {
	        //CHECK_ERROR_CODE(esp_task_wdt_delete(NULL), ESP_OK);
	        free(poll_event);
	        vTaskDelete(NULL);
	    }
	uart_send_queue = xQueueCreate(MAX_POLL_QUEUE_SIZE,sizeof(modbus_poll_event_t));
	// First Poll to initialize the queue
	uint8_t curr_slave = 1;
    uint16_t quantity = 2;
	poll_event->slave =  curr_slave;
	poll_event->function = READ_INPUT;
	poll_event->data.starting_addrs= curr_slave;
	xQueueSend(uart_send_queue,poll_event,pdMS_TO_TICKS(TIME_SCAN));

	while(1){
		if(xQueueReceive(uart_send_queue,poll_event,pdMS_TO_TICKS(TIME_SCAN))){
			ESP_LOGI(TAG, "Function %d, slave %d",poll_event->function, poll_event->slave);
			switch(poll_event->function){
				case READ_INPUT:
					read_input_register(poll_event->slave, (uint16_t)poll_event->slave, quantity);
					break;
				case WRITE_SINGLE_COIL:
					write_single_coil(poll_event->slave, poll_event->data.coil_state);
					break;
				default:
					ESP_LOGW(TAG, "Function not implemented YET!");
			}
		}else{
			// If no external polls, just read the input register
	        if (curr_slave >= SLAVES) /// If we reach the maximum quantity of slave, start over
	            curr_slave = 0;
			curr_slave++;
			poll_event->slave =  curr_slave;
			poll_event->function = READ_INPUT;
			poll_event->data.starting_addrs = curr_slave;
			xQueueSend(uart_send_queue,poll_event,pdMS_TO_TICKS(TIME_SCAN));
		}
        CHECK_ERROR_CODE(esp_task_wdt_reset(), ESP_OK);
	}
}

void test_RS485_RH18(void *arg) {
    ESP_LOGI(TAG, "Modbus Master Task initialized");
    CHECK_ERROR_CODE(esp_task_wdt_add(NULL), ESP_OK);
    CHECK_ERROR_CODE(esp_task_wdt_status(NULL), ESP_OK);
    QueueHandle_t uart_queue;
    uart_event_t event;
    uint8_t *slave_response = (uint8_t *)malloc(RX_BUF_SIZE);
    modbus_registers[1]     = &inputRegister[0];
    uart_init(&uart_queue);


    while (1) {
        if (xQueueReceive(uart_queue, (void *)&event,
                          (portTickType)MODBUS_TIMEOUT)) {
        	ESP_LOGW(TAG_UART, "EVENTTTT TYPE: %d EVENT SIZEEEE %d",event.type,event.size);
            switch (event.type) {
            case UART_DATA:
                if (uart_read_bytes(UART_NUM_1, slave_response, event.size,
                                    (portTickType)MODBUS_TIMEOUT) == ESP_FAIL) {
                    ESP_LOGE(TAG_UART, "Error while reading UART data");
                    break;
                }
                ESP_LOGI(TAG_UART, "Response received is:");
                ESP_LOG_BUFFER_HEX(TAG_UART, slave_response, event.size);
                if (CRC16(slave_response, event.size) == 0) {
                	test_led_RH12();
                    ESP_LOGI(TAG, "Modbus frame verified");
                    if (check_exceptions(slave_response) == ESP_FAIL)
                        break;
                    save_register(slave_response, event.size, modbus_registers);
                } else {
                    ESP_LOGI(TAG, "Frame not verified CRC : %d",
                             CRC16(slave_response, event.size));
                    test_led_RH12();
                    uart_flush(UART_NUM_1);
                }
                break;
            case UART_BREAK:
            	break;
            case UART_FIFO_OVF:
                ESP_LOGI(TAG, "hw fifo overflow");
                uart_flush_input(UART_NUM_1);
                xQueueReset(uart_queue);
                break;
            case UART_BUFFER_FULL:
                ESP_LOGI(TAG, "ring buffer full");
                uart_flush_input(UART_NUM_1);
                xQueueReset(uart_queue);
                break;
            case UART_FRAME_ERR:
                ESP_LOGI(TAG, "uart frame error");
                uart_flush_input(UART_NUM_1);
                xQueueReset(uart_queue);
                uart_init(&uart_queue);
                break;
            default:
                ESP_LOGE(TAG, "uart event type: %d", event.type);
                break;
            }
        } else {
            ESP_LOGI(TAG, "Timeout");
        }
        CHECK_ERROR_CODE(esp_task_wdt_reset(), ESP_OK);
    }
    free(slave_response);
    vTaskDelete(NULL);
}

