#include "esp_log.h"
#include "test_led.h"
#include "esp_task_wdt.h"
#include "flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"
#include "global_variables.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "string.h"
#include <stdio.h>
#include <stdlib.h>
#include <test_led.h>
#include <test_LoRa.h>
#include "Config_UART.h"
#include <test_RS485.h>
#include "CRC.h"
#define Pila 1024
#define tamCOLA 1
#define tamMSN 1
xQueueHandle Cola_UART;
uint8_t* datoRX;

#define PULSES_KW         225
#define PULSE_COUNTER = 0
#define LIMIT_ERROR_COUNT 10

//#define RX_BUF_SIZE 1024
//#define TX_BUF_SIZE 1024

#define TIME_SCAN  2000
#define MAX_SLAVES 255
#define SLAVES 10

#define TWDT_TIMEOUT_S 20
#define TWDT_RESET     5000
#define MODBUS_TIMEOUT 100 // in ticks == 1 s

#define BUF_LORA_SIZE 5

#define CLOSE_RELAY 18
#define OPEN_RELAY  23
#define PULSE_TIME  1000 // ms
static char *TAG      = "INFO";

#define CHECK_ERROR_CODE(returned, expected)                                   \
    ({                                                                         \
        if (returned != expected) {                                            \
            ESP_LOGE(TAG, "TWDT ERROR\n");                                     \
            abort();                                                           \
        }                                                                      \
    })


#define MAX_POLL_QUEUE_SIZE 5  // Max quantity of polls in queue

nvs_address_t pulse_address;
QueueHandle_t lora_queue;
QueueHandle_t uart_send_queue;
// key length 32 bytes for 256 bit encrypting, it can be 16 or 24 bytes for 128
// and 192 bits encrypting mode

/*unsigned char key[] = {0xff, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                       0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
                       0xff, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                       0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};*/

void app_main(){
	ESP_LOGI(TAG, "MCU initialized");
	ESP_LOGI(TAG, "Init Watchdog");
	CHECK_ERROR_CODE(esp_task_wdt_init(TWDT_TIMEOUT_S, true), ESP_OK);
	configGPIO();
	test_led_RH11();
//	test_LoRa_RH7();

	xTaskCreatePinnedToCore(&test_led_RH14,"test_led_RH14", Pila*5, NULL, 5, NULL,0);

    ESP_LOGI(TAG, "Start Modbus master task");
    xTaskCreatePinnedToCore(&test_RS485_RH18, "task_modbus_master", 2048 * 2,NULL, 10, NULL, 1);
    xTaskCreatePinnedToCore(&modbus_master_poll, "modbus_master_poll", 2048 * 2,NULL, 10, NULL, 1);

}

