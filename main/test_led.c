#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include <string.h>
#include "freertos/queue.h"
#include "esp_log.h"
// Definiciones ----------------------------//
#define LED 2
#define bufferRX 128
#define received_pulse GPIO_NUM_27
#define tamBUFFER 1024
static char *TAG      = "INFO";


int TP5 = 0;

int TP13 = 0;

void configGPIO(){
	gpio_set_direction(LED,GPIO_MODE_INPUT_OUTPUT);
	gpio_set_direction(received_pulse,GPIO_MODE_INPUT_OUTPUT);
	gpio_set_level(received_pulse,0);
}

void test_led_RH11(void){
	ESP_LOGI(TAG, "INICIA LED");
	int counter = 0;
	while(counter <= 5){
			gpio_set_level(LED,!gpio_get_level(LED));
			vTaskDelay(250/portTICK_PERIOD_MS);
			counter +=1;
			TP5++;
		}
	vTaskDelay(500/portTICK_PERIOD_MS);
}

void test_led_RH12(void){
	ESP_LOGI(TAG, "Recibe trama   12222");
	gpio_set_level(LED,!gpio_get_level(LED));
	vTaskDelay(500/portTICK_PERIOD_MS);
	gpio_set_level(LED,!gpio_get_level(LED));
	vTaskDelay(500/portTICK_PERIOD_MS);
	TP5 = 1;
}


void test_led_RH13(void){
	ESP_LOGI(TAG, "Recibe trama");
	int counter = 0;
	while(counter <= 3){
			gpio_set_level(LED,!gpio_get_level(LED));
			vTaskDelay(250/portTICK_PERIOD_MS);
			counter +=1;
		}
	TP5 = 2;
	vTaskDelay(500/portTICK_PERIOD_MS);
}

static void test_impulses_RH15(uint32_t pulses){
	pulses ++;
	TP13++;
	ESP_LOGW(TAG, "Pulse number %d", pulses);
}

void test_led_RH14(void* P){
	ESP_LOGW(TAG, "Prueba PULSOS");
	uint32_t pulses = 0;
	int is_pulse = 0;
	for(;;){
		if(gpio_get_level(received_pulse)){
			vTaskDelay(11/portTICK_PERIOD_MS);
			if(gpio_get_level(received_pulse)){
				test_impulses_RH15(pulses);
			}
			gpio_set_level(LED,!gpio_get_level(LED));
			vTaskDelay(500/portTICK_PERIOD_MS);
			gpio_set_level(LED,!gpio_get_level(LED));
			is_pulse = 1;
		}
		if(is_pulse == 1){
			is_pulse = 0;
		}else{
			vTaskDelay(500/portTICK_PERIOD_MS);
		}
	}
}


